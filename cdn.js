//
// Shinobi
// Copyright (C) 2016 Moe Alam, moeiscool
//
//
// # Donate
//
// If you like what I am doing here and want me to continue please consider donating :)
// PayPal : paypal@m03.ca
//
process.on('uncaughtException', function (err) {
    console.error('uncaughtException',err);
});
var fs = require('fs');
var express = require('express');
var app = express();
var http = require('http');
var server = http.createServer(app);
var exec = require('child_process').exec;
var config = require('./conf.json');
if(config.amplitudeApiToken){
    console.log('Amplitude Tracking is Enabled')
    var amplitudeEndpoint = 'https://api.amplitude.com/httpapi?api_key='+config.amplitudeApiToken+'&event='
    var trackEvent = function(type,eventData,callback){
        if(!type)type = 'Untitled Event'
        if(!eventData)eventData = null
        if(typeof eventData === 'function'){
            var callback = eventData
            var eventData = null
        }
        var sendData = [
            {
              "user_id": config.TelegramSender + ' ('+ config.baywatchAccountId +')',
              "event_type": type,
              "event_properties": eventData
            }
        ]
        var eventDataForUrl = encodeURIComponent(JSON.stringify(sendData))
        exec("curl -v '" + amplitudeEndpoint + eventDataForUrl + "'",function(err,data){
            if(err)console.log(err,data)
            if(callback)callback(err,data)
        })
    }
    app.use(function (req, res, next) {
        next()
        trackEvent('Request',{
            "Page": req.originalUrl,
            "Protocol": req.protocol,
            "IP": req.headers['cf-connecting-ip']||req.headers["CF-Connecting-IP"]||req.headers["'x-forwarded-for"]||req.connection.remoteAddress
        })
    })
}else{
    console.log('Amplitude Tracking is Disabled')
}

server.listen(80,function(){
    console.log('CDN on Port 80');
});
app.enable('trust proxy');
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});
app.use('/',express.static(__dirname + '/files'));
app.get('/', function (req,res){
    res.sendFile(__dirname+'/index.html');
});
